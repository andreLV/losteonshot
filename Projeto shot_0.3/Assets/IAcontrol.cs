﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IAcontrol : MonoBehaviour
{
    public player player_s;
    public input input_s;
    public selecionavel     selecionavel_s;
    public playerSelect     playerSelect_s;
    public controleDeTurno  controleDeTurn_s;
    public controle_batalha controleBatalha_s;
    public dano dano_s;
    public movimentacao movimentacao_s;

    public bool started = false;

    
    public void startIA_f()
    {
        //if (!started  && controleDeTurn_s.getEndTurn_f() == false && controle_batalha.checkTurn_f(controle_batalha.codeTEnemie_f())) 
        if (!started)
        {
            StartCoroutine(startIA());
        }
    }

    IEnumerator startIA()
    {
        started = true;              
        GameObject[] heros = controleBatalha_s.getHeros_f();
        int attackHero = Random.Range(0,heros.Length);

        yield return new WaitForSeconds(1);
        input_s.setPointPosition_f(utilitarios.worldToScreenPoint(Camera.main,transform.position));
        movimentacao_s.start_f();
        yield return new WaitForSeconds(1);

        for (int a = 0; a < 2; a++)
        {
            yield return new WaitForSeconds(0.01f);
            Vector2 target = utilitarios.worldToScreenPoint(Camera.main, heros[attackHero].transform.position);
            Vector2 mp = input_s.getPointPosition_f();
            Vector2 difs = new Vector2(mp.x - target.x, mp.y - target.y);
            Vector2 newP = new Vector2(mp.x - difs.x * 1.2f * Time.deltaTime, mp.y - difs.y * 1.2f * Time.deltaTime);

            input_s.setPointPosition_f(newP);
            float dis = Vector3.Distance(heros[attackHero].transform.position,movimentacao_s.getHit_f().point);            
            if (dis >= (player_s.getRange_f()))
            {
                a = 0;
            }
        }
        yield return new WaitForSeconds(0.5f);
        input_s.setMouseButton_f(0, true);
        yield return new WaitForSeconds(0.1f);
        input_s.setMouseButton_f(0, false);

        for (int a = 0; a < 2; a++)
        {
            yield return new WaitForSeconds(0.01f);            
            if (player_s.getMovimentacao_f().getRuning_f())
            {
                a = 0;
            }
        }
        yield return new WaitForSeconds(0.4f);

        if ( Vector3.Distance( heros[attackHero].transform.position, transform.position) <= player_s.getRange_f()){

            dano_s.attack_f(heros[attackHero]);
        }
        else
        {
            controleDeTurn_s.endMyTurn_f();
        }

        yield return new WaitForSeconds(1);
        StartCoroutine(controleBatalha_s.escolherVezInimigo_f(0.01f));
        this.enabled = false;
        started = false;
    }


}
