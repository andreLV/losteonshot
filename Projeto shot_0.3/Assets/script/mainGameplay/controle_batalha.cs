﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class controle_batalha : MonoBehaviour
{




    public GameObject screenChangeTurn_v;
    //---------------------------------------------------------------------------------------------------
    //---------------------------------------------------------------------------------------------------
    
    private static int codeTurn_enemie =  2;
    private static int codeTurn_hero = 1;
    private static int turn_v = codeTurn_hero;
    //---

    public GameObject[] heros_v;
    public GameObject[] enemies_v;
    //---------------------------------------------------------------------------------------------------
    //---------------------------------------------------------------------------------------------------




    private static int status_gameplay_heroTurn_select = 21,
        status_gameplay_heroTurn_andar = 12,
        status_gameplay_heroTurn_actionMenu = 221;
    //status_gameplay_heroTurn_selectPointRun = 110;

    private static int status_gameplay = status_gameplay_heroTurn_select;
    //---
    public static int get_status_gameplay_f()
    {
        return status_gameplay;
    }
   
    public static int get_status_heroTurn_actionMenu_f()
    {
        return status_gameplay_heroTurn_actionMenu;
    }
    public static int get_status_heroTurn_select_f()
    {
        return status_gameplay_heroTurn_select;
    }
    public static int get_status_heroTurn_andar_f()
    {
        return status_gameplay_heroTurn_andar;
    }
    //--
    public static void set_status_gameplay_f(int value)
    {
        status_gameplay = value;
    }
    //---
    public static bool compare_status_gameplay_f(int value)
    {

        if (status_gameplay == value)
            return true;
        else
            return false;

    }
    //---------------------------------------------------------------------------------------------------





    //--
    public GameObject[] getEnemies_f()
    {
        return enemies_v;
    }
    public GameObject[] getHeros_f()
    {
        return heros_v;
    }
    //--
    public void removeEnemie_f(GameObject value)
    {
        
        enemies_v = addArray.removeArrayGameObject_f(enemies_v, value);
        //-------------
        checkWinner_f();
    }
    public void removeHero_f(GameObject value)
    {
       heros_v = addArray.removeArrayGameObject_f(heros_v, value);
        //-------------
        checkWinner_f();
    }

    bool endBattle_v;
    public void checkWinner_f()
    {
        if(enemies_v.Length == 0)
        {
            endBattle_v = true;
            StartCoroutine(addScreenChangeTurn_f(Color.green, "YOU WIN"));
        }
        if (heros_v.Length == 0)
        {
            endBattle_v = true;
            StartCoroutine(addScreenChangeTurn_f(Color.red, "YOU LOSE"));
        }
    }

    

    public void checkChangeTurn_f()
    {


        if (endBattle_v) { return; }

        int numHeroEndTurn = 0;
        int numEnemieEndTurn = 0;

        for (int a = 0; a < heros_v.Length; a++)
        {
            controleDeTurno ct = heros_v[a].GetComponent<controleDeTurno>();
            if (ct.getEndTurn_f())
            {
                numHeroEndTurn++;
            }
        }
        for (int a = 0; a < enemies_v.Length; a++)
        {
            controleDeTurno ct = enemies_v[a].GetComponent<controleDeTurno>();
            if (ct.getEndTurn_f())
            {
                numEnemieEndTurn++;
            }
        }
      

        if (numHeroEndTurn >= heros_v.Length && checkTurn_f(codeTurn_hero))  
        {
            
            //turno inimigo
            setTurnEnemie_f();
            StartCoroutine(resetTurn_f(codeTurn_enemie));
            StartCoroutine(resetTurn_f(codeTurn_hero));
            StartCoroutine("pauseActionHero_f");
            StartCoroutine(addScreenChangeTurn_f(Color.red, "TURNO INIMIGO"));
            StartCoroutine(escolherVezInimigo_f(4));
        }
        
        if (numEnemieEndTurn >= enemies_v.Length && checkTurn_f(codeTurn_enemie))
        {
            //turno hero
            setTurnHero_f();
            StartCoroutine(resetTurn_f(codeTurn_enemie));
            StartCoroutine(resetTurn_f(codeTurn_hero));
            StartCoroutine(addScreenChangeTurn_f(Color.green, "SEU TURNO"));
           
        }
        
        
    }

    GameObject[] inimigosValidos_v;
    public IEnumerator escolherVezInimigo_f(float seconds)
    {

        yield return new WaitForSeconds(seconds);
        inimigosValidos_v = new GameObject[0];
        for (int a = 0; a < enemies_v.Length; a++)
        {

            if (enemies_v[a].GetComponent<controleDeTurno>().getEndTurn_f() == false)
            {
                inimigosValidos_v = addArray.addGameObjectInArray_f(inimigosValidos_v, enemies_v[a]);
            }            
        }

        if (inimigosValidos_v.Length > 0)
        {
            int rvez = Random.Range(0, inimigosValidos_v.Length);
            inimigosValidos_v[rvez].GetComponent<IAcontrol>().startIA_f();
        }

    }

    IEnumerator addScreenChangeTurn_f(Color cor,string texto)
    {
        yield return new WaitForSeconds(2);
        GameObject sc = Instantiate(screenChangeTurn_v);
        Text text = sc.transform.Find("an_turn_change/Text").GetComponent<Text>();
        text.color = cor;
        text.text = texto;
        sc.transform.SetParent(GameObject.Find("Canvas").transform);
        sc.GetComponent<RectTransform>().localPosition = Vector3.zero;
        sc.transform.localScale = new Vector3(1, 1, 1);
        sc.GetComponent<RectTransform>().sizeDelta = Vector3.zero;
    }
    

    IEnumerator resetTurn_f(int codevalue)
    {
        yield return new WaitForSeconds(4);
        GameObject[] arr = heros_v;
        if (codevalue == codeTurn_enemie)
        {
            arr = enemies_v;
        }
        for (int a = 0; a < arr.Length; a++)
        {
            arr[a].GetComponent<controleDeTurno>().setEndTurn_f(false);
            arr[a].GetComponent<selecionavel>().setPause_f(false);
            arr[a].GetComponent<selecionavel>().myTurnMode_f();
            arr[a].GetComponent<player>().resetMovimento_f();
        }
    }

    IEnumerator pauseActionHero_f()
    {
        yield return new WaitForSeconds(4);
        for (int a = 0; a < heros_v.Length; a++)
        {

            heros_v[a].GetComponent<selecionavel>().setPause_f(true);
           
        }
    }



        //--------------------------------
        public static bool checkTurn_f(int value)
    {
        bool t = false;
        if(turn_v == value)
        {
            t = true;
        }
        return t;
    }
    public static void setTurnHero_f()
    {
        turn_v = codeTurn_hero;
    }
    public static void setTurnEnemie_f()
    {
        turn_v = codeTurn_enemie;       
    }
    public static int codeTEnemie_f()
    {
        return codeTurn_enemie;
    }
    public static int codeTHero_f()
    {
        return codeTurn_hero;
    }
    //--------------------------------


}
