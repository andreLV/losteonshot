﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class menuActionList : MonoBehaviour
{


    public void menuPlayerOptionSelect_terminar_f(selecionavel sl, playerSelect ps, player pl, GameObject rangeImage)//botao cancelar no menu quando seleciona o personagem
    {
        sl.unSelect_f();
        sl.setPause_f(true);
        pl.getControleTurno_f().endMyTurn_f();
        ps.endSelection_f();
        pl.hideRange_f(rangeImage);
        Destroy(gameObject);
    }
    public void menuPlayerOptionSelect_cancelar_f(selecionavel sl,playerSelect ps,player pl,GameObject rangeImage)//botao cancelar no menu quando seleciona o personagem
    {
        sl.unSelect_f();
        sl.setPause_f(false);
        ps.endSelection_f();
        pl.hideRange_f(rangeImage);
        Destroy(gameObject);
    }
    public void menuPlayerOptionSelect_andar_f(movimentacao mv, player pl,Button bt)//botao andar no menu quando seleciona o personagem
    {   
        mv.start_f();       
        Destroy(gameObject);
    }
    public void menuPlayerOptionSelect_atacar_f(selecionavel sl, playerSelect ps, player pl, GameObject rangeImage)//botao andar no menu quando seleciona o personagem
    {
        //sl.unSelect_f();
        //sl.setPause_f(false);
        //ps.endSelection_f();
        //rangeImage.GetComponent]<Image>
        //--
        GameObject.FindWithTag("mira").GetComponent<mouse>().targetMode_f(pl);
        Destroy(gameObject);
        
    }

}
