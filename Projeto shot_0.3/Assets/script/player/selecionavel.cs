﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class selecionavel : MonoBehaviour
{
    public Outline[] outline_v;
    private string type_v;
    public bool selected_v;
    private playerSelect ps_s;
    private bool pause_v;
    private Color defaultColor_v;
    private Color endTurnColor_v;

    void Awake()
    {
        defaultColor_v = outline_v[0].GetComponent<Renderer>().material.color;
        endTurnColor_v =  new Color(defaultColor_v.r * 0.3f, defaultColor_v.g * 0.3f, defaultColor_v.b * 0.3f,1);
        

        if (GetComponent<playerSelect>())
            ps_s = GetComponent<playerSelect>();
    }

    private void OnMouseEnter()
    {
        if (pause_v) { return; }
        select_f(Color.white,2,"over");
    }
    private void OnMouseExit()
    {
        if (pause_v) { return; }
        unSelect_f();
    }
    private void OnMouseDown()
    {

        if (pause_v) { return; }

        ps_s.selectAction_f(this);
    }

    public void setPause_f(bool value)
    {
        pause_v = value;
    }


    public void select_f(Color cor,float width,string type)
    {
        
        type_v = type;
        for (int a = 0; a < outline_v.Length; a++)
        {
            outline_v[a].enabled = true;
            outline_v[a].OutlineColor = cor;
            outline_v[a].OutlineWidth = width;
        }
        selected_v = true;
    }
    public void unSelect_f()
    {
        for(int a = 0; a < outline_v.Length; a++)
        {
            outline_v[a].enabled = false;
        }
        selected_v = false;
        type_v = string.Empty;
    }
    public string getType_f()
    {        
        return type_v;
    }
    public bool getSelected_f()
    {
        return selected_v;
    }
    public void endTurnMode_f()
    {
        for (int a = 0; a < outline_v.Length; a++)
        {
            outline_v[a].GetComponent<Renderer>().material.color = endTurnColor_v;
        }
    }
    public void myTurnMode_f()
    {
        for (int a = 0; a < outline_v.Length; a++)
        {
            outline_v[a].GetComponent<Renderer>().material.color = defaultColor_v;
        }
    }
}
