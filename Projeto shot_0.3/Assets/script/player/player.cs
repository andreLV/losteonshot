﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class player : MonoBehaviour
{

    private maindata maindata_s;
    private main main_s;
    [SerializeField] private string name_v = "Lee", classe_v = "Espada Chim";
    [SerializeField] private float range_v = 2;
    [SerializeField] private int danoFisico_v = 50, danoMagico_v = 0, danoArma_v = 20;

    //----------------------------------------------------------
    [SerializeField] private int[] hp_v =       { 100, 100 }; //hp
    [SerializeField] private int[] pa_v =       { 100, 100 }; //ponto de habiidade
    [SerializeField] private int[] armadura_v = { 20, 20 };   //ponto de armadura
    [SerializeField] private int[] resistenciaMagica_v = { 0, 0 };   //absove resistencia magica
    [SerializeField] private float[] movimento_v = { 15, 15 };//unidade de movimento
    //----------------------------------------------------------

    public GameObject rangeIndicator_v;


   
    private hp hp_s;
    private selecionavel selecionavel_s;
    private playerSelect playerSelect_s;
    private movimentacao movimentacao_s;
    private input input_s;
    private dano dano_s;
    private controleDeTurno controleDeTurno_s;





    private void Awake()
    {

        main_s = GameObject.FindWithTag("main").GetComponent<main>();
        maindata_s = main_s.getMainData_f();

        if (GetComponent<selecionavel>()) { selecionavel_s = GetComponent<selecionavel>(); }            
        if (GetComponent<playerSelect>()) { playerSelect_s = GetComponent<playerSelect>();  }            
        if (GetComponent<movimentacao>()) { movimentacao_s = GetComponent<movimentacao>(); }           
        if (GetComponent<input>()) { input_s = GetComponent<input>(); }
        if (GetComponent<dano>()) { dano_s = GetComponent<dano>(); }
        if (GetComponent<hp>()) { hp_s = GetComponent<hp>(); }
        if (GetComponent<controleDeTurno>()) { controleDeTurno_s = GetComponent<controleDeTurno>(); }



    }
    public main getMain_f()
    {
        return main_s;
    }
    public dano getDano_s()
    {
        return dano_s;
    }
    public maindata getMainData_f()
    {
        return maindata_s;
    }
    public playerSelect getPlayerSelect_f()
    {
        return playerSelect_s;
    }
    public selecionavel getSelecionavel_f()
    {
        return selecionavel_s;
    }
    public movimentacao getMovimentacao_f()
    {
        return movimentacao_s;
    }
    public input getInput_f()
    {
        return input_s;
    }




    public string getName_f()
    {
        return name_v;
    }
    public string getClass_f()
    {
        return classe_v;
    }
    public float getRange_f()
    {
        return range_v;
    }
    public int getDanoFisico_f()
    {
        return danoFisico_v;
    }
    public int getDanoMagico_f()
    {
        return danoMagico_v;
    }
    public int getHp_f()
    {
        return hp_v[0];
    }
    public int getArmadura_f()
    {
        return armadura_v[0];
    }
    public int getResistenciaMagica_f()
    {
        return resistenciaMagica_v[0];
    }
    public int getDanoArma_f()
    {
        return danoArma_v;
    }


    public float getMovimento_f()
    {
        return movimento_v[0];
    }
    public float getMaxMovimento_f()
    {
        return movimento_v[1];
    }
    public void resetMovimento_f()
    {
        movimento_v[0] = movimento_v[1];
    }

    public controleDeTurno getControleTurno_f()
    {
        return controleDeTurno_s;
    }

    public bool checkMaxMovimento_f(float distance)
    {
        if(distance >= getMovimento_f())
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    
    public GameObject showRange_f(Transform point,Color cor)
    {
        GameObject rangeIndicator_a;
        //if (rangeIndicator_a) {print("ERROR: Range indicador ja existe"); return null; }
        rangeIndicator_a = Instantiate(rangeIndicator_v, point.position,Quaternion.identity);
        rangeIndicator_a.name = "circleRange";
        rangeIndicator_a.transform.localScale = new Vector3(range_v,range_v,range_v);
        rangeIndicator_a.transform.SetParent(point);
        rangeIndicator_a.transform.Find("range/circle").gameObject.GetComponent<Renderer>().material.color = cor;
        rangeIndicator_a.transform.Find("range/line").gameObject.GetComponent<Renderer>().material.color = new Color(cor.r,cor.g,cor.b,1);
        rangeIndicator_a.transform.Find("range/circle").gameObject.AddComponent<rangeCircle>().setDono_f(gameObject,maindata_s);
     

        return rangeIndicator_a;
    }
    public void hideRange_f(GameObject  obj)
    {
        if (!obj) { print("ERROR: Range indicador NÃO existe"); return; }
        Destroy(obj);
    }


    
    public void updatePassos_f(float distance)
    {

        float prev = movimento_v[0];
        prev -= distance;
  
        if (prev >= 0.0f)
        {
        
            movimento_v[0] = prev;
        }
        
        
    }

    public int calcDano_f()//calcula seu dano
    {
        int dano = getDanoFisico_f() + getDanoArma_f() + getDanoMagico_f();
        return dano;
    }

    public void damage_f(int dano)//leva dano
    {
        dano -= getResistenciaMagica_f();
        dano -= getArmadura_f();
        hp_v[0] -= dano;
        if(hp_v[0] <= 0)
        {
            hp_v[0] = 0;
        }
        hp_s.updateHP_f(hp_v[0]);
    }

    public int getMaxHp_f()
    {
        return hp_v[1];
    }

}
