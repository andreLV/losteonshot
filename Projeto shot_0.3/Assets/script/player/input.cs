﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class input : MonoBehaviour
{

    public bool human_v = true;

    
    private Vector3 mPoint_v;
    private bool[] mouseButton_v = { false, false, false };    
    private bool[] waitMouseUp_v = { false, false, false };



    void Start()
    {
        
    }
    void Update()
    {
        if (!human_v) {

            for (int a = 0; a < mouseButton_v.Length; a++)
            {
                waitMouseUp_v[a] = false;
            }
            return;
        }
        mPoint_v = Input.mousePosition;
        //--
        for (int a = 0; a < mouseButton_v.Length; a++)
        {
            mouseButton_v[a] = Input.GetMouseButton(a);
        }
        //---
        for (int a = 0; a < mouseButton_v.Length; a++)
        {
            if (waitMouseUp_v[a] && Input.GetMouseButtonUp(a))
            {
                waitMouseUp_v[a] = false;
            }
        }
    }

    public void setMouseButton_f(int position,bool value)
    {
        mouseButton_v[position] = value;
    }
   

    public void setPointPosition_f(Vector3 value)
    {
        mPoint_v = value;
    }

    public Vector3 getPointPosition_f()
    {
        return mPoint_v;
    }

    

    public bool getMouseButtonDown_f(int numButton)
    {
        bool retorno = false;
        if (!waitMouseUp_v[numButton]) {
            if (mouseButton_v[numButton]) { retorno = true; waitMouseUp_v[numButton] = true;};
        };
        return retorno;
    }
    public bool getMouseButton_f(int numButton)
    {
        return mouseButton_v[numButton];
    }
    



}
