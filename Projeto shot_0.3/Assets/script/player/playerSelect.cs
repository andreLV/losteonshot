﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class playerSelect : MonoBehaviour
{
    private main main_s;
    public GameObject selectCircle_v;
    private GameObject selectCircle_a;
    private GameObject menu_a;
    private player player_s;
    private movimentacao movimentacao_s;
    private GameObject rangeSelect_v;

    void Awake()
    {
        if (GetComponent<player>())
            player_s = GetComponent<player>();
        if (GetComponent<movimentacao>())
            movimentacao_s = GetComponent<movimentacao>();        
        if (GameObject.FindWithTag("main") && GameObject.FindWithTag("main").GetComponent<main>())
            main_s = GameObject.FindWithTag("main").GetComponent<main>();
    }
   
    public void selectAction_f(selecionavel sl)
    {

        if (GameObject.Find("menu_button")) { return; }

        if (CompareTag("Player"))
        {
            Color dcb = main_s.getColors().get_cor_button_float_dialog_f();
            addCircleSelect_f();
            sl.setPause_f(true);
            createMenuSelect_f(sl);
            rangeSelect_v = player_s.showRange_f(transform, new Color(0.5f, 1, 1, 0.05f));
        }
    }

    void createMenuSelect_f(selecionavel sl)
    {        
        Color dcb = main_s.getColors().get_cor_button_float_dialog_f();
        menu_a = main_s.getCreateImage_f().createMenu_f(5, new string[] { "Terminar","Cancelar", "Defender", "Atacar", "Andar" }, new Color[] { main_s.getColors().get_cor_negative_button_float_dialog_f(), main_s.getColors().get_cor_negative_button_float_dialog_f(), dcb, dcb, dcb }, Input.mousePosition, null, 130, 50);
        Vector3 alvo = utilitarios.worldToScreenPoint(Camera.main, transform.position);
        menu_a.transform.position = alvo;        
        positionMenuUpdate psu = menu_a.AddComponent<positionMenuUpdate>();        
        psu.startPosition(transform, new Vector2(-30, -20), 4);

        menuActionList ml = menu_a.AddComponent<menuActionList>();
        ml.transform.Find("Terminar").GetComponent<Button>().onClick.AddListener(() => ml.menuPlayerOptionSelect_terminar_f(sl, this, player_s, rangeSelect_v));
        ml.transform.Find("Cancelar").GetComponent<Button>().onClick.AddListener(() => ml.menuPlayerOptionSelect_cancelar_f(sl,this,player_s,rangeSelect_v));
        ml.transform.Find("Atacar").GetComponent<Button>().onClick.AddListener(() => ml.menuPlayerOptionSelect_atacar_f(sl, this, player_s, rangeSelect_v));
        GameObject btAndar_v = ml.transform.Find("Andar").gameObject;
        GameObject btAtacar_v = ml.transform.Find("Atacar").gameObject;
        //------------------------------------atacar
        AtacarConfigButton_f(btAtacar_v);
        //------------------------------------

        //--------------------------------------------------------------------------------------andar
        andarConfigButton_f(btAndar_v,ml);
        //---------------------------------------------------------------------------------------------

    }

    public void createMenuConfirmDestination_f(selecionavel sl)
    {
        if(player_s.getInput_f().human_v == false) { return; };


        Color dcb = main_s.getColors().get_cor_button_float_dialog_f();
        menu_a = main_s.getCreateImage_f().createMenu_f(5, new string[] { "Terminar","Parar", "Defender", "Atacar", "Andar" }, new Color[] { main_s.getColors().get_cor_negative_button_float_dialog_f(), main_s.getColors().get_cor_negative_button_float_dialog_f(), dcb, dcb, dcb }, Input.mousePosition, null, 130, 50);
        Vector3 alvo = utilitarios.worldToScreenPoint(Camera.main, transform.position);
        menu_a.transform.position = alvo;
        positionMenuUpdate psu = menu_a.AddComponent<positionMenuUpdate>();
        psu.startPosition(transform, new Vector2(-30, -20), 4);

        menuActionList ml = menu_a.AddComponent<menuActionList>();

        ml.transform.Find("Terminar").GetComponent<Button>().onClick.AddListener(() => ml.menuPlayerOptionSelect_terminar_f(sl, this, player_s, rangeSelect_v));

        ml.transform.Find("Parar").GetComponent<Button>().onClick.AddListener(() => ml.menuPlayerOptionSelect_cancelar_f(sl, this, player_s, rangeSelect_v));

        ml.transform.Find("Atacar").GetComponent<Button>().onClick.AddListener(() => ml.menuPlayerOptionSelect_atacar_f(sl, this, player_s, rangeSelect_v));

        GameObject btAndar_v = ml.transform.Find("Andar").gameObject;
        GameObject btAtacar_v = ml.transform.Find("Atacar").gameObject;

        //------------------------------------atacar
        AtacarConfigButton_f(btAtacar_v);
        //------------------------------------

        //--------------------------------------------------------------------------------------andar
        andarConfigButton_f(btAndar_v, ml);
        //---------------------------------------------------------------------------------------------

    }



    void AtacarConfigButton_f(GameObject bt)
    {

        bt_attack_check_enable scr = bt.AddComponent<bt_attack_check_enable>();
        scr.set_f(player_s, bt.GetComponent<Button>(),main_s);

        /*if (player_s.getDano_s().getEnemiesSelected_f().Length == 0)
        {
            bt.GetComponent<Button>().enabled = false;
            bt.GetComponent<Image>().color = new Color(0.3f, 0.3f, 0.3f, 1);
        }*/


    }


    void andarConfigButton_f(GameObject btAndar_v, menuActionList ml)
    {
        if (player_s.getMovimento_f() <= 0.2f)
        {
            btAndar_v.GetComponent<Button>().enabled = false;
            btAndar_v.GetComponent<Image>().color = new Color(0.3f, 0.3f, 0.3f, 1);
        }
        else
        {
            btAndar_v.GetComponent<Button>().onClick.AddListener(() => ml.menuPlayerOptionSelect_andar_f(movimentacao_s, player_s, ml.transform.Find("Andar").GetComponent<Button>()));
        }
    }

    


    public void endSelection_f()
    {
        if (selectCircle_a)
            Destroy(selectCircle_a);
    }
    void addCircleSelect_f()
    {
        if (selectCircle_a) { Destroy(selectCircle_a); }

        selectCircle_a = Instantiate(selectCircle_v);
        Vector3 p = transform.position;
        selectCircle_a.transform.position = new Vector3(p.x, p.y - 0.43f, p.z);
        selectCircle_a.GetComponent<SpriteRenderer>().color = Color.white;
        selectCircle_a.transform.SetParent(transform);
    }

}
