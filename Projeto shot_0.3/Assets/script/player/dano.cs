﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class dano : MonoBehaviour
{


    private player player_s;
    private GameObject[] enemiesSelected_v;

    void Start()
    {
        enemiesSelected_v = new GameObject[0];
        player_s = GetComponent<player>();
    }

    public void resetEnemiesSelected_f()
    {
        enemiesSelected_v = new GameObject[0];

    }
    
    public void incremmentEnemiesSelected_f(GameObject value)
    {
        bool add = true;
        for (int a = 0; a < enemiesSelected_v.Length; a++)
        {
            if(enemiesSelected_v[a].GetInstanceID() == value.GetInstanceID())
            {
                add = false;
            }
        }
        if (add)
        {
            enemiesSelected_v = addArray.addGameObjectInArray_f(enemiesSelected_v, value);
        }
    }
    public GameObject[] getEnemiesSelected_f()
    {
        return enemiesSelected_v;
    }
    public void removeEnemiesSelect_f(GameObject value)
    {
        GameObject[] e = new GameObject[0];

        for (int a = 0; a < enemiesSelected_v.Length; a++)
        {
            if (value != enemiesSelected_v[a])
            {
                e = addArray.addGameObjectInArray_f(enemiesSelected_v, value);
            }
        }
        enemiesSelected_v = e;
    }
    public bool checkObjInSelected_f(GameObject enemie)
    {
        bool founded = false;

        for(int a = 0; a < enemiesSelected_v.Length; a++)
        {
            if(enemiesSelected_v[a].GetInstanceID() == enemie.GetInstanceID())
            {
                founded = true;
            }
        }
        return founded;


    }

    public bool attack_f(GameObject enemie)
    {
        int dano = player_s.calcDano_f();
        enemie.GetComponent<player>().damage_f(dano);
        player_s.getControleTurno_f().endMyTurn_f();
        return true;

    }
}
