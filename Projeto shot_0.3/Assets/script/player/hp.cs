﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class hp : MonoBehaviour
{
    private player player_s;
    private createImage createImage_s;
    public Color barrColor_v;
    public Color damageBarrColor_v;
    private float maxHP_v;
    private float hp_v;

    private maindata maindata_s;
    private GameObject iconAttack_v;
    private GameObject iconAttack_a;
    private RectTransform hpBarr_v;
    private RectTransform prebarr_v;
    public Animation an_v;

    private void Start()
    {
        createImage_s = GameObject.FindWithTag("main").GetComponent<createImage>();
        hpBarr_v = createImage_s.createBarr_f(Color.black, barrColor_v, Color.grey, damageBarrColor_v, transform, new Vector2(50, 10), new Vector2(10, 10));
        player_s = GetComponent<player>();
        maindata_s = player_s.getMainData_f();
        iconAttack_v = maindata_s.get_pf_utility_f(maindata_s.code_pf_icon_espada_v);
        prebarr_v = hpBarr_v.transform.parent.Find("preBarr").GetComponent<RectTransform>();
    }

    private dano danoDono_s;

    private void OnTriggerStay(Collider other)
    {
       
        if (other.CompareTag("Range"))
        {
            if (CompareTag("Enemie"))
            {
                if (other.gameObject.GetComponent<rangeCircle>().getDono_f().CompareTag("Player"))
                {
                    StopCoroutine("destroyIcon_f");
                    danoDono_s = other.gameObject.GetComponent<rangeCircle>().getDono_f().GetComponent<dano>();
                    danoDono_s.incremmentEnemiesSelected_f(gameObject);
                    if (!iconAttack_a)
                    {
                        
                        
                        iconAttack_a = Instantiate(iconAttack_v, transform.position, Quaternion.identity);
                        iconAttack_a.transform.Translate(iconAttack_a.transform.up * 1);
                        iconAttack_a.transform.Translate(iconAttack_a.transform.forward * 2);
                        player_s.getSelecionavel_f().select_f(Color.red,4,"target");

                    }
                   
                    StartCoroutine("destroyIcon_f",0.02f);

                }
            }
        }
    }

    IEnumerator destroyIcon_f(float time)
    {
        yield return new WaitForSeconds(time);
        danoDono_s.resetEnemiesSelected_f();
        player_s.getSelecionavel_f().unSelect_f();
        Destroy(iconAttack_a);
    }

    public void updateHP_f(int hp)
    {
        //---------------------------------------------------------------------------------------------------
        RectTransform rt_back_barr = hpBarr_v.transform.parent.GetComponent<RectTransform>();
        float value = proporcao.getProporcao_f(hp, 0, player_s.getMaxHp_f(), 0, rt_back_barr.sizeDelta.x);
        //---------------------------------------------------------------------------------------------------
        hpBarr_v.sizeDelta = new Vector2(value, rt_back_barr.sizeDelta.y);
        Vector2 np = hpBarr_v.transform.localPosition;
        np.x = (hpBarr_v.sizeDelta.x - rt_back_barr.sizeDelta.x) / 2;
        hpBarr_v.transform.localPosition = np;
        //---------------------------------------------------------------------------------------------------
        if(hp <= 0)
        {
            dead_f();
        }
    }

    private void Update()
    {
        if (hpBarr_v)
        {
            Vector2 p = prebarr_v.localPosition;
            Vector2 s = prebarr_v.sizeDelta;
            //--------------------------------------
            float pDifX = p.x - hpBarr_v.transform.localPosition.x;
            float sDifX = s.x - hpBarr_v.sizeDelta.x;

            p.x -= pDifX * 4 * Time.deltaTime;
            s.x -= sDifX * 4 * Time.deltaTime;

            prebarr_v.transform.localPosition = p;
            prebarr_v.sizeDelta = s;
        }
       
    }

    void dead_f()
    {

        if(player_s.getInput_f().human_v == false)
        {
            player_s.getMain_f().getControleBatalha_f().removeEnemie_f(gameObject);
        }
        else
        {
            player_s.getMain_f().getControleBatalha_f().removeHero_f(gameObject);
        }

        an_v.Play("dead");
        Destroy(hpBarr_v.transform.parent.transform.parent.gameObject, 2.9f);
        Destroy(gameObject, 3);
    }

}
