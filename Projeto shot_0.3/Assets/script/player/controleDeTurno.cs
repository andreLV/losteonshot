﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class controleDeTurno : MonoBehaviour
{

    private bool endMyTurn_v = false;
    private player player_s;


    void Start()
    {
        player_s = GetComponent<player>();
    }

    public bool getEndTurn_f()
    {
        return endMyTurn_v;
    }
    public void setEndTurn_f(bool value)
    {

        endMyTurn_v = value;

    }

    //---------------------------
    public void endMyTurn_f()
    {
    
        endMyTurn_v = true;
        player_s.getSelecionavel_f().endTurnMode_f();
        player_s.getMain_f().getControleBatalha_f().checkChangeTurn_f();
            

    }
   


}
