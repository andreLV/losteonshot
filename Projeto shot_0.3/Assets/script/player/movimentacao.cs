﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.UI;

public class movimentacao : MonoBehaviour
{

    public main main_s;
    private player player_s;
    private playerSelect playerSelect_s;
    private input input_s;
    private LineRenderer lr_s;
    //--
    public GameObject effectPointClick_v;
    public GameObject text_v;    
    //--
    private NavMeshAgent agent_v;
    private Vector3 startDestination_v;
    private Transform targetDestination_v;
    private Transform lastDestination_v;    
    //--

    private bool runing_v;
    private bool paused_v = true;


    private float lastTargetDistance_v;
    private float distanceHit_v;
    private float distance_v;
    private bool maxDistanceExeded_v;
    private bool destinoTracado_v;


    

    void Awake()
    {

        if (GameObject.FindWithTag("main"))
        {
            main_s = GameObject.FindWithTag("main").GetComponent<main>();
        }
        agent_v = GetComponent<NavMeshAgent>();
        lr_s = GetComponent<LineRenderer>();            
        lr_s.enabled = false;
        if (GetComponent<player>())
            player_s = GetComponent<player>();
        

    }
    
    private void Start()
    {
         input_s = GetComponent<input>();
        if (player_s)
        {
            playerSelect_s = player_s.getPlayerSelect_f();
        }
    }

    public void start_f()//chamado em menuActionList, momento em que clica em andar
    {
       

        //------------------
        if (!lastDestination_v)
        {
            GameObject ld = new GameObject("lastDestination "+name);
            lastDestination_v = ld.GetComponent<Transform>();
        }
        if (!targetDestination_v)
        {
            GameObject ld = new GameObject("targetDestination "+name);
            targetDestination_v = ld.GetComponent<Transform>();
        }
       
        targetDestination_v.position = transform.position;
        lastDestination_v.position = transform.position;
        //---------------------------------------------------------------------------------------------------------------
        Vector3[] caminho = calcular.caminho(new NavMeshPath(), transform.position, transform.position);
        lr_s.positionCount = caminho.Length;
        lr_s.SetPositions(caminho);
        maxDistanceExeded_v = false;
        //---------------------------------------------------------------------------------------------------------------
       

        StartCoroutine(checkStart_f());
    }

    bool enableStart = false;
    IEnumerator checkStart_f()
    {
        
        enableStart = false;
        Ray ray = Camera.main.ScreenPointToRay(input_s.getPointPosition_f());
        RaycastHit hit;
        
        if (Physics.Raycast(ray, out hit))
        {
            
            if (Vector3.Distance(transform.position, hit.point) < player_s.getMovimento_f())
            {
                enableStart = true;
                
            }
        }

        yield return new WaitForSeconds(0.01f);

       
        if (enableStart == false)
        {
            StartCoroutine(checkStart_f());
        }
        else
        {
            
            lr_s.enabled = true;
            player_s.showRange_f(targetDestination_v, new Color(1, 1, 1, 0.05f));
            paused_v = false;
        }
    }
    
    public void end_f()
    {        
        runing_v = false;
        lr_s.enabled = false;
        paused_v = true;
        destinoTracado_v = false;

        playerSelect_s.createMenuConfirmDestination_f(player_s.getSelecionavel_f());


        
        if (targetDestination_v)
        player_s.hideRange_f(targetDestination_v.gameObject);
        if(lastDestination_v)
        player_s.hideRange_f(lastDestination_v.gameObject);
        

    }

    public bool getRuning_f()
    {
        return runing_v;
    }
    private RaycastHit hit;

    public RaycastHit getHit_f()
    {
        return hit;
    }
    void Update()
    {



        if (paused_v) { return; }
        Ray ray = Camera.main.ScreenPointToRay(input_s.getPointPosition_f());
        
        if (Physics.Raycast(ray, out hit))
        {


            distanceHit_v = Vector3.Distance(transform.position, hit.point);
            
            if (lastDestination_v)
            {                
                lastTargetDistance_v = Vector3.Distance(transform.position, lastDestination_v.position);
            }

            //------------------------------------------------------------ ESCOLHER DESTINO
            if (hit.point != null && input_s.getMouseButtonDown_f(0) && !runing_v && targetDestination_v && Vector3.Distance(transform.position, targetDestination_v.position) > 0.2f)
            {
                
                startDestination_v = transform.position;
                setDestination_f();                
            }
            //------------------------------------------------------------
            //------------------------------------------------------------VERIFICANDO MAXIMO DE DISTANCIA PARA MUDAR COR DO LINE
            if (maxDistanceExeded_v && !runing_v)
            {
                lr_s.endColor = Color.yellow;
                lr_s.startColor = Color.yellow;                
            }

            //------------------------------------------------------------------------------------------------------------ 

            
            if (!maxDistanceExeded_v)
            {               
                if (targetDestination_v)
                {
                    targetDestination_v.position = hit.point;
                    
                    if (!runing_v)
                    {
                        addOrChangeTextDistance_f(targetDestination_v);
                    }
                }
                Vector3 lasPoint = hit.point;//trance o line renderer no mouse
                Vector3 start = transform.position;

                lr_s.endColor = Color.cyan;
                lr_s.startColor = Color.cyan;
                if (runing_v)  //se eu estiver correndo, tranque o line renderer na rota      
                {
                    lasPoint = lastDestination_v.position;
                    lr_s.enabled = false;
                }
                else
                {
                    startDestination_v = start;
                }
            
            Vector3[] caminho = calcular.caminho(new NavMeshPath(),startDestination_v, lasPoint);
            lr_s.positionCount = caminho.Length;
            lr_s.SetPositions(caminho);
            }
            //------------------------------------------------------------------------------------------------------------
        }
        //lastTargetDistance_v = Vector3.Distance(transform.position, lastDestination_v.position);
        checkRunning_f();



        
            if (!destinoTracado_v)
            {
                if (!runing_v)
                {
                    distance_v = distanceHit_v;
                }
            else{
                    distance_v = lastTargetDistance_v;
                }
                maxDistanceExeded_v = checkMaxDistance_f();
            }
            else
            {
                distance_v = Vector3.Distance(transform.position, lastDestination_v.position);
                addOrChangeTextDistance_f(lastDestination_v);
                //player_s.updatePassos_f(distance_v);
                maxDistanceExeded_v = false;
            }
        

    }


    void setDestination_f()
    {   
        agent_v.SetDestination(targetDestination_v.position);
        Instantiate(effectPointClick_v, targetDestination_v.position, Quaternion.identity);
        lastDestination_v.position = targetDestination_v.position;
        lr_s.enabled = false;
        destinoTracado_v = true;
        player_s.hideRange_f(targetDestination_v.gameObject);
        if (maxDistanceExeded_v)
        {
            player_s.updatePassos_f(player_s.getMovimento_f());
        }
        else
        {
            player_s.updatePassos_f(distance_v);
        }
    }
    GameObject textMetros_a;
    void checkRunning_f()
    {
        if (agent_v.velocity.magnitude == 0.0f && runing_v)//END DESTINATION 
        {
            end_f();
        }
        if (agent_v.velocity.magnitude != 0.0f && !runing_v)//RUNING TO DESTINATION 
        {
            //lr_s.enabled = true;
            runing_v = true;                        
            player_s.showRange_f(lastDestination_v,new Color(0.4f,1,0.4f,0.02f));//no momento que ele clica  para escolher o destino
        }
    }
   

    textDistance textDistance_s;
    void addOrChangeTextDistance_f(Transform targetPosition)
    {
        if (!textMetros_a)
        {
            textMetros_a = Instantiate(text_v);
            textMetros_a.transform.SetParent(GameObject.Find("Canvas").transform);
            textMetros_a.GetComponent<Text>().fontSize = 35;
            textDistance_s = textMetros_a.AddComponent<textDistance>();
        }
        if (!textDistance_s.compareTextDistance_f(targetPosition))
        {
            textDistance_s.start_f(targetPosition,player_s);
        }
    }
    public bool checkMaxDistance_f()
    {
        if (player_s.checkMaxMovimento_f(distance_v))
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    public bool getMaxDistanceExeded_f()
    {
        return maxDistanceExeded_v;
    }
    public float getDistance_f()
    {
        return distance_v;
    }

    public float getDistanceHit_f()
    {
        return distanceHit_v;
    }
    public float getLastTargetDistance()
    {
        return lastTargetDistance_v;
    }


}
