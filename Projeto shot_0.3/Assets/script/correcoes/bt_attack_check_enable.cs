﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class bt_attack_check_enable : MonoBehaviour
{

    private Button bt_s;
    private player player_s;
    private main main_s;

    

    public void set_f(player pl,Button bt,main main)
    {
        player_s = pl;
        bt_s = bt;
        main_s = main;
    }

    void Update()
    {

        if (!player_s) { return; }
        
        if (player_s.getDano_s().getEnemiesSelected_f().Length == 0)
        {
            bt_s.enabled = false;
            bt_s.GetComponent<Image>().color = new Color(0.3f, 0.3f, 0.3f, 1);
        }
        else
        {
            bt_s.enabled = true;
            bt_s.GetComponent<Image>().color = main_s.getColors().get_cor_button_float_dialog_f();
        }

    }
}
