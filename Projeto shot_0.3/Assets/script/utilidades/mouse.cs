﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class mouse : MonoBehaviour
{

    public Image sr_s;
    public Sprite spriteSeta_v;
    public Sprite spriteMira_v;
    private player plAtacante_s;

    void Start()
    {
        Cursor.visible = false;
    }

    bool targetMode_v;
    void Update()
    {
        transform.position = (Input.mousePosition);


        if (!targetMode_v) { return; }

        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;


        if (Physics.Raycast(ray, out hit))
        {
            if (plAtacante_s.GetComponent<dano>().checkObjInSelected_f(hit.collider.gameObject))
            {
                if(plAtacante_s.GetComponent<input>().getMouseButtonDown_f(0))
                {
                    if (plAtacante_s.GetComponent<dano>().attack_f(hit.collider.gameObject))
                    {
                        selectMode_f(plAtacante_s);
                    }
                }
            }
        }
    }

    public void targetMode_f(player pl)
    {
        plAtacante_s = pl;
        targetMode_v = true;
        sr_s.sprite = spriteMira_v;
    }
    public void selectMode_f(player plAtacante)// plAtacante_s é o jogador atacante
    {
        targetMode_v = false;
        sr_s.sprite = spriteSeta_v;
        plAtacante_s.GetComponent<selecionavel>().unSelect_f();
        plAtacante_s.GetComponent<playerSelect>().endSelection_f();
        Destroy(plAtacante_s.transform.Find("circleRange").gameObject);
        //plAtacante.getControleTurno_f().endMyTurn_f();
    }

}
