﻿using UnityEngine;
using System.Collections;

public class segue : MonoBehaviour {

	
	public GameObject target_v;//obj a ser seguido
	public GameObject pointDisTarget_v;//ponto de distancia, caso n setado, sem problemas ele setara automaticamente e usado em mp e pp setando um gameObject diferente
	public float limitSegueDistance_v;//se existe limite de distancia para que ele siga o target se n kizer isso deixe 0
	public float sensibilityx_v;//sensibilidade horizontal (velocida)
	public float sensibilityy_v;//sensibilidade vertical (velocida)
    public float sensibilityz_v;//sensibilidade Profundidade (velocida)
    public float discontx;//disconto de posicaox
	public float disconty;//disconto de posicaoy
    public float discontz;//disconto de posicaoy
    public bool  cordenadasEnablex_v;//habilitar seguir o x do obj
	public bool  cordenadasEnabley_v;//habilitar seguir o y do obj
    public bool cordenadasEnablez_v;//habilitar seguir o y do obj
    public string tagNameTarget_v; // tag do obj ( caso escreva a tag n e necessario setar o target_v manualmente )
	public float invert_v = 1;
	public float limitSegueDown_v;//limite baixo
	public float limitSegueUp_v;//limite cima
	public float limitSegueRigth_v;//limite direita
	public float limitSegueLeft_v; //limite esquerda 
    public float limitSegueFront_v; //limite frente
    public float limitSegueBack_v; //limite traz
    private bool blockX;//acionado quando obj seguido chega em um dos limites horizontal
	private bool blockY;//acionado quando obj seguido em um dos limites bertical
    private bool blockZ;//acionado quando obj seguido em um dos limites Profundidade
    public bool seguindo_v;
	
	void Awake(){
		target_f();
    }


	public void target_f(){

		//se nao houver target setado proucure no palco pela tag tagNameTarget
		if(!target_v && tagNameTarget_v != ""){
			target_v = GameObject.FindGameObjectWithTag (tagNameTarget_v).gameObject;
		}

	}
	public float distanciaTarget_v;
	void Update () {


		if(!enabled){return;}

		if(!target_v){return;}else{
			if(!pointDisTarget_v){
				pointDisTarget_v = target_v.gameObject;
			}
		}

		distanciaTarget_v = Vector3.Distance(pointDisTarget_v.transform.position,target_v.transform.position);

		if(limitSegueDistance_v > 0){
			if(distanciaTarget_v > limitSegueDistance_v){
				seguindo_v = false;
				return;
			}
		}
		seguindo_v = true;
		Vector3 p = transform.position;
		Vector3 pt = target_v.transform.position;

		//--------------para de seguir caso chegue em determinada posicao------------

		if( (limitSegueUp_v !=0 && pt.y >= limitSegueUp_v) ||  (limitSegueDown_v !=0 && pt.y <= limitSegueDown_v) ){
			blockY = true;
		}else{
			blockY = false;
		}

		if( (limitSegueRigth_v !=0 && pt.x >= limitSegueRigth_v) ||  (limitSegueLeft_v !=0 && pt.x <= limitSegueLeft_v) ){
			blockX = true;
		}else{
			blockX = false;
		}

        if ((limitSegueFront_v != 0 && pt.z >= limitSegueFront_v) || (limitSegueBack_v != 0 && pt.z <= limitSegueBack_v))
        {
            blockZ = true;
        }else{
            blockZ = false;
        }

        //----------------------------------------------------------------


        if (cordenadasEnablex_v && !blockX){		
			float disX = p.x - target_v.transform.position.x + discontx;
			p.x -= disX * sensibilityx_v * Time.deltaTime;
		}

		if(cordenadasEnabley_v && !blockY){
			float disY = p.y - target_v.transform.position.y + disconty;
			p.y -= disY * sensibilityy_v * Time.deltaTime;		
		}
        if (cordenadasEnablez_v && !blockZ)
        {
            float disZ = p.z - target_v.transform.position.z + discontz;
            p.z -= disZ * sensibilityz_v * Time.deltaTime;
        }
        transform.position = p;
	
	}
	
}
