﻿
using UnityEngine;
using System.Collections;

public class destroy : MonoBehaviour {

	public float seconds_v = 1;	
	public bool rpc_v;
	public Transform unParentOnDestroy_v;
    public GameObject target_v;


	void Start () {

        if (!target_v)
        {
            target_v = gameObject;
        }
		if (seconds_v >= 0) {			
			StartCoroutine(destroy_f ());
		}


	}

	IEnumerator destroy_f(){
		yield return new WaitForSeconds (seconds_v);
		action_f ();
	}

	public void action_f(){

		
			Destroy (target_v.gameObject, seconds_v);
		
		if (unParentOnDestroy_v) {
			unParentOnDestroy_v.SetParent (null);
		}
	}

	
	 
	
	
	
		
	

	
	
}
