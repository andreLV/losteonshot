﻿using UnityEngine;
using System.Collections;

public class segueInstantaneo : MonoBehaviour {

	public Transform target_v;
	public bool rotationCopy_v = true;
	public bool xenable_v = true;
	public bool yenable_v = true;
	public bool zenable_v = true;

	void Update () {

		if(!target_v){return;}

		Vector3 tp = transform.position;
		if(xenable_v){
			tp.x = target_v.position.x;
		}
		if(yenable_v){
			tp.y = target_v.position.y;
		}
		if(zenable_v){
			tp.z = target_v.position.z;
		}
		transform.position = tp;


		if(rotationCopy_v){
			Quaternion rt = target_v.rotation;
			transform.rotation = rt;
		}


	}
}
