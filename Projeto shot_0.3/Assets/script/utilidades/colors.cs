﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class colors : MonoBehaviour
{
    public Color cor_background_float_dialog_v;
    public Color cor_text_float_dialog_v;
    public Color cor_button_float_dialog_v;
    public Color cor_positive_button_float_dialog_v;
    public Color cor_negative_button_float_dialog_v;


    public Color get_cor_background_float_dialog_f()
    {
        return cor_background_float_dialog_v;
    }
    public Color get_cor_text_float_dialog_f()
    {
        return cor_text_float_dialog_v;
    }
    public Color get_cor_button_float_dialog_f()
    {
        return cor_button_float_dialog_v;
    }
    public Color get_cor_positive_button_float_dialog_f()
    {
        return cor_positive_button_float_dialog_v;
    }
    public Color get_cor_negative_button_float_dialog_f()
    {
        return cor_negative_button_float_dialog_v;
    }
}
