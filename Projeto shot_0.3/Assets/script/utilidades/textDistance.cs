﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class textDistance : MonoBehaviour
{

    private Text text_s;
    RectTransform rt_s;
    Transform targetPosition_v;
    private player player_s;
    private float distance_v;
    private float discontY = 30;
    private float discontX = 30;
    private bool start_v = false;

    void Awake()
    {
        rt_s = GetComponent<RectTransform>();
        text_s = GetComponent<Text>();
    }

    public void start_f(Transform value,player pl)
    {
        targetPosition_v = value;
        start_v = true;
        player_s = pl;
    }

    public void changeDestination_f(Transform value)
    {
        targetPosition_v = value;
    }

    void Update()
    {
        if (!start_v) { return; }
        if (!targetPosition_v) { Destroy(gameObject); return; }

        rt_s.position = utilitarios.worldToScreenPoint(Camera.main, targetPosition_v.position);
        Vector3 positionText = rt_s.position;
        positionText.y += discontY;
        positionText.x += discontX;
        rt_s.position = positionText;
        textMovimento_f();


    }


    void textMovimento_f()
    {
        
        if (player_s)
        {
            
            string htmlColor;   
            distance_v = player_s.getMovimentacao_f().getDistance_f();
            if (player_s.getMovimentacao_f().getMaxDistanceExeded_f())
            {                
                htmlColor = "#FF0000";
                distance_v = player_s.getMovimento_f();//max
            }
            else
            {
                htmlColor = "#00FFFF";
            }
            text_s.text = "<color=" + htmlColor + ">" + distance_v.ToString("F2") + "</color> / " + player_s.getMovimento_f().ToString("F2") + " M";
        }
    }

    public bool compareTextDistance_f(Transform targetP)
    {
        if (!targetPosition_v) { return false; }

        if(targetPosition_v.GetInstanceID() == targetP.GetInstanceID())
        {
            return true;
        }
        else
        {
            return false;
        }
    }

}
