﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class positionMenuUpdate : MonoBehaviour
{


    private bool start = false;
    private Transform target;
    private float speed = 4;
    private Vector2 discont;

    public void startPosition(Transform tr,Vector2 ds,float vel)
    {
        target = tr;
        start = true;
        discont = ds;
        speed = vel;
    }

    void Update()
    {
        if(!start) { return; }

        Vector3 alvo = utilitarios.worldToScreenPoint(Camera.main, target.position);
        float difx =  transform.position.x - alvo.x + discont.x;
        float dify =  transform.position.y - alvo.y + discont.y;
        Vector3 p = transform.position;
        p.x -= difx * speed * Time.deltaTime;
        p.y -= dify * speed * Time.deltaTime;
        transform.position = p;        

    }
}
