﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class createImage : MonoBehaviour
{
    public main main_s;
    public GameObject button_s;
    private Transform canvasTr_v;


    void Awake()
    {
        canvasTr_v = GameObject.Find("Canvas").transform;        
        //Color dcb = main_s.getColors().get_cor_button_float_dialog_f();
        //createMenu_f(4, new string[] { "Cancelar", "Defender", "Atacar","Andar" }, new Color[] { main_s.getColors().get_cor_negative_button_float_dialog_f(), dcb, dcb, dcb }, new Vector3(0, 0, 0), null, 130,50);
    }
   
    public GameObject createImage_f(string name,Color cor,Vector3 pointScreen, Transform parent, float width, float heigth)
    {
        Transform canvas = canvasTr_v.transform;
        GameObject img = new GameObject(name);
        img.AddComponent<RectTransform>();
        if (!parent) { img.transform.transform.SetParent(canvas); }
        img.AddComponent<CanvasRenderer>();
        Image image_s = img.AddComponent<Image>();
        image_s.color = cor;
        image_s.transform.localScale = new Vector3(1, 1, 1);        
        image_s.rectTransform.sizeDelta = new Vector2(width, heigth);        
        image_s.rectTransform.position = utilitarios.worldToScreenPoint(Camera.main, pointScreen);
        return img;
    }


    public GameObject createMenu_f(int numButtons,string[] name, Color[] cor, Vector3 pointScreen, Transform parent,float width,float heigth)
    {
        float plusPositionY = 0;
        GameObject menu = new GameObject("menu_button");
        menu.AddComponent<RectTransform>();
        //GameObject back = createImage_f("backmenuButton", main_s.getColors().get_cor_background_float_dialog_f(), pointScreen, null, width, heigth * numButtons);
        //GameObject[] buttons_a = new GameObject[0];
        for (int a = 0; a < numButtons; a++)
        {            
            GameObject obj = Instantiate(button_s);            
            Button bt  = obj.GetComponent<Button>();
            bt.transform.Find("Text").GetComponent<Text>().text = name[a];
            bt.transform.Find("Text").GetComponent<Text>().color = new Color(cor[a].r * 0.2f, cor[a].g * 0.2f, cor[a].b * 0.2f);
            obj.name = name[a];
            if (!parent) { obj.transform.transform.SetParent(canvasTr_v); }
            obj.transform.localScale = new Vector3(1, 1, 1);
            Image img = obj.GetComponent<Image>();
            img.color = cor[a];
            img.rectTransform.position = utilitarios.worldToScreenPoint(Camera.main, pointScreen);
            img.rectTransform.sizeDelta = new Vector2(width, heigth);
            Vector3 position = img.rectTransform.localPosition;            
            position.y += plusPositionY;
            img.rectTransform.localPosition = position;
            plusPositionY += heigth;
            //buttons_a = addArray.addGameObjectInArray_f(buttons_a, obj);
            if (a == 0)
            {
                menu.transform.SetParent(canvasTr_v);
                menu.transform.GetComponent<RectTransform>().localPosition = obj.transform.GetComponent<RectTransform>().localPosition;
            }
            obj.transform.SetParent(menu.transform);
        }
        return menu;
    }

    public RectTransform createBarr_f(Color contentBackColor,Color barrColor,Color edgeColor,Color damageColor, Transform targetPosition,Vector2 size,Vector2 edge)
    {
        float plusPositionY = -16;


        //--edge add
        GameObject edgeBarr = new GameObject("edge_barr");
        RectTransform rt_edge = edgeBarr.AddComponent<RectTransform>();
        edgeBarr.AddComponent<Image>().color = edgeColor;
        edgeBarr.transform.SetParent(canvasTr_v);
        //--background add
        GameObject contentBarr = new GameObject("background_barr");
        RectTransform rt_content = contentBarr.AddComponent<RectTransform>();
        contentBarr.AddComponent<Image>().color = contentBackColor;
        contentBarr.transform.SetParent(edgeBarr.transform);
        //--pre Barr add
        GameObject preBarr = new GameObject("preBarr");
        RectTransform rt_preBarr = preBarr.AddComponent<RectTransform>();
        preBarr.AddComponent<Image>().color = damageColor;
        preBarr.transform.SetParent(contentBarr.transform);
        //--barr add
        GameObject barr = new GameObject("barr");
        RectTransform rt_barr = barr.AddComponent<RectTransform>();
        barr.AddComponent<Image>().color = barrColor;
        barr.transform.SetParent(contentBarr.transform);
        barr.transform.localPosition = Vector3.zero;
        //--

        //--position update--------------
        positionMenuUpdate pmu = edgeBarr.AddComponent<positionMenuUpdate>();
        pmu.startPosition(targetPosition, new Vector2(0, plusPositionY), 12);
        Vector3 alvo = utilitarios.worldToScreenPoint(Camera.main, targetPosition.position);
        edgeBarr.transform.position = alvo;
        //---------------------------------------
        //--
        edgeBarr.transform.localScale = new Vector3     (1, 1, 1);
        contentBarr.transform.localScale = new Vector3  (1, 1, 1);
        barr.transform.localScale = new Vector3     (1, 1, 1);
        //--
        rt_edge.sizeDelta = new Vector2 (size.x+edge.x,size.y+edge.y);
        rt_content.sizeDelta = size;
        rt_barr.sizeDelta = size;
        rt_preBarr.sizeDelta = rt_barr.sizeDelta;
        rt_preBarr.transform.localPosition = rt_barr.localPosition;


        /*
       rt_barr.sizeDelta = new Vector2(40, rt_content.sizeDelta.y);
       Vector2 np = rt_barr.transform.localPosition;
       np.x = (rt_barr.sizeDelta.x - rt_content.sizeDelta.x) / 2 ;
       rt_barr.transform.localPosition = np;*/

        return rt_barr;


    }





}
