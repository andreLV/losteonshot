﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class calcular : MonoBehaviour
{

    public static Vector3[] caminho(NavMeshPath path,Vector3 initialPosition,Vector3 target)
    {
        Vector3[] retorno = null;        
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);        
        NavMesh.CalculatePath(initialPosition, target, NavMesh.AllAreas, path);            
        retorno = path.corners;       
        return retorno;
    }
    public static float distancia_f(Vector3[] positions)
    {
        float dis = 0;
        for (int a = 0; a < positions.Length; a++)
        {
            if (a + 1 <= positions.Length - 1)
            {
                dis += Vector3.Distance(positions[a], positions[a + 1]);
            }
        }
        return dis;
    }
    
    /*
     para testar coloque o seguinte codigo no start de qualquer script, se de o valor -5 é porque está correto diante dos valores testes colocados nos argumentos da função abaixo
     deveria dar a metade de de -10 (outMin) que seria -5 pois a proporção corrente atual de 50, exatamente a metade de 0 inMin á 100 inMax.
     print( proporcao.getProporcao_f(50,0,100,-10,0));
    */
    public static float proporcao_f(float currentValue, float inMin, float inMax, float outMin, float outMax)
    {
        float newValue = (currentValue - inMin) * (outMax - outMin) / (inMax - inMin) + outMin;
        return newValue;
    }

}





