﻿using UnityEngine;
using System.Collections;

public class utilitarios : MonoBehaviour {


	void Start () {
	
	}
	
	public static void moveOrder_f(Transform tr,int delta,Transform orderPoint){
		int index = orderPoint.GetSiblingIndex ();
		tr.SetSiblingIndex (index + delta);
	}


    public static Vector2 worldToScreenPoint(Camera cam, Vector3 worldPoint)
    {
        if ((Object)cam == (Object)null)
            return new Vector2(worldPoint.x, worldPoint.y);
        return (Vector2)cam.WorldToScreenPoint(worldPoint);
    }
    public static Vector3 screeToWordPoint(Camera cam, Vector2 screenPoint)
    {        
        return cam.ScreenToWorldPoint(screenPoint);
    }


}
