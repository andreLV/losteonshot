﻿using UnityEngine;
using System.Collections;

public class addArray : MonoBehaviour {

	public static GameObject[] addGameObjectInArray_f(GameObject[] arrayObj,GameObject newObj)
	{
		GameObject[] arrayObjR = new GameObject[arrayObj.Length+1];
		for(int a = 0; a < arrayObjR.Length; a++)
		{
			if (a < arrayObj.Length)
			{
				arrayObjR[a] = arrayObj[a];
			}
			else
			{
				arrayObjR[a] = newObj;
			}
		}
		return arrayObjR;

	}
    public static Vector3[] addVector3InArray_f(Vector3[] arrayObj, Vector3 newObj)
    {
        Vector3[] arrayObjR = new Vector3[arrayObj.Length + 1];
        for (int a = 0; a < arrayObjR.Length; a++)
        {
            if (a < arrayObj.Length)
            {
                arrayObjR[a] = arrayObj[a];
            }
            else
            {
                arrayObjR[a] = newObj;
            }
        }
        return arrayObjR;

    }
    public static bool[] addBoolInArray_f(bool[] arrayObj, bool newObj)
    {
        bool[] arrayObjR = new bool[arrayObj.Length + 1];
        for (int a = 0; a < arrayObjR.Length; a++)
        {
            if (a < arrayObj.Length)
            {
                arrayObjR[a] = arrayObj[a];
            }
            else
            {
                arrayObjR[a] = newObj;
            }
        }
        return arrayObjR;

    }
    public static Color[] addColorInArray_f(Color[] arrayObj, Color newObj)
    {
        Color[] arrayObjR = new Color[arrayObj.Length + 1];
        for (int a = 0; a < arrayObjR.Length; a++)
        {
            if (a < arrayObj.Length)
            {
                arrayObjR[a] = arrayObj[a];
            }
            else
            {
                arrayObjR[a] = newObj;
            }
        }
        return arrayObjR;

    }
    public static int[] addIntInArray_f(int[] arrayObj,int newObj)
	{
		int[] arrayObjR = new int[arrayObj.Length+1];
		for(int a = 0; a < arrayObjR.Length; a++)
		{
			if (a < arrayObj.Length)
			{
				arrayObjR[a] = arrayObj[a];
			}
			else
			{
				arrayObjR[a] = newObj;
			}
		}
		return arrayObjR;

	}
	public static string[] addStringInArray_f(string[] arrayObj,string newObj)
	{
		string[] arrayObjR = new string[arrayObj.Length+1];
		for(int a = 0; a < arrayObjR.Length; a++)
		{
			if (a < arrayObj.Length)
			{
				arrayObjR[a] = arrayObj[a];
			}
			else
			{
				arrayObjR[a] = newObj;
			}
		}
		return arrayObjR;

	}
	public static Transform[] addTransformInArray_f(Transform[] arrayObj,Transform newObj)
	{
		Transform[] arrayObjR = new Transform[arrayObj.Length+1];
		for(int a = 0; a < arrayObjR.Length; a++)
		{
			if (a < arrayObj.Length)
			{
				arrayObjR[a] = arrayObj[a];
			}
			else
			{
				arrayObjR[a] = newObj;
			}
		}
		return arrayObjR;

	}


    public static GameObject[] removeArrayGameObject_f(GameObject[] array,GameObject obj)
    {
        GameObject[] add = new GameObject[array.Length-1];
        int count = 0;
        for (int a = 0; a < array.Length; a++)
        {
            if (array[a].GetInstanceID() != obj.GetInstanceID())
            {                
                add[count] = array[a];
                count++;
            }
        }
        return add;
    }


}
