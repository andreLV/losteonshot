﻿using UnityEngine;
using System.Collections;

public class proporcao : MonoBehaviour {


    /*
    para testar coloque o seguinte codigo no start de qualquer script, se de o valor -5 é porque está correto diante dos valores testes colocados nos argumentos da função abaixo
    deveria dar a metade de de -10 (outMin) que seria -5 pois a proporção corrente atual de 50, exatamente a metade de 0 inMin á 100 inMax.
    print( proporcao.getProporcao_f(50,0,100,-10,0));
    */

    public static float getProporcao_f(float currentValue, float inMin , float inMax, float outMin, float outMax) {

        float newValue = (currentValue - inMin) * (outMax - outMin) / (inMax - inMin) + outMin;
        return newValue;
	}
    


}
