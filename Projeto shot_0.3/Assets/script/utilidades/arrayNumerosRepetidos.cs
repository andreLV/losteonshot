﻿using UnityEngine;
using System.Collections;

public class arrayNumerosRepetidos : MonoBehaviour {
	

	public static int[] check_f(int[] numeros){
		int[] numerosRepetidos = new int[numeros.Length];
		for (int i = 0; i < numeros.Length; i++)
		{	     
			for (int j = 0; j < numeros.Length ; j++)	        
			{	        
				if (numeros [i] == numeros [j] && i != j) {	            					
					numerosRepetidos [i] = numeros [i];		
				}
				if (numeros [i] != numeros [j] && i != j) {	            					
					numerosRepetidos [i] = -999;	
				}
			}	        
		}
		return numerosRepetidos;	                
	}
    public static int[] checkPure_f(int[] numeros)
    {
        int[] numerosRepetidos = new int[numeros.Length];
        for (int i = 0; i < numeros.Length; i++)
        {
            for (int j = 0; j < numeros.Length; j++)
            {
                if (numeros[i] == numeros[j])
                {
                    numerosRepetidos[i] = numeros[i];
                }               
            }
            print("numeros repetidos" + numerosRepetidos[i]);
        }
        return numerosRepetidos;
    }

}