﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class moveCameraMouse : MonoBehaviour
{
    public float vel_v = 50;
    public float margim = 20;
    private segue segue_s;

    void Start()
    {
        segue_s = GetComponent<segue>();
    }

    void Update()
    {


        Vector2 mp = Input.mousePosition;
        if(Input.GetMouseButton(2))
        {
            segue_s.target_v = null;
        }
        else
        {
            return;
        }
        


        Vector2 center = new Vector2(Screen.width / 2, Screen.height / 2);
        Ray ray = Camera.main.ScreenPointToRay(mp);
        RaycastHit hit;
        if (Physics.Raycast(ray, out hit))
        {
            if (hit.point != null)
            {
                Vector3 p = transform.position;
                float difX = p.x - hit.point.x;
                //float difY = p.y - hit.point.y;
                float difZ = p.z - hit.point.z +15;
                p.x -= difX * vel_v * Time.deltaTime;
                //p.y -= difY * 1.2f * Time.deltaTime;
                p.z -= difZ * vel_v * Time.deltaTime;
                transform.position = p;

            }

        }

        
         /*
            Vector3 p = transform.position;
            Vector3 mpConvert = Camera.main.WorldToScreenPoint(mp);
            float difX = p.x - mpConvert.x;
            float difY = p.y - mpConvert.y;
            p.x -= difX * 2 * Time.deltaTime;
            p.y -= difY * 2 * Time.deltaTime;
            transform.position = p;
            */

            /*
            if (mp.x <= margim)
            {
                transform.Translate(Vector3.left * (Vector2.Distance(new Vector2(Screen.width/2,Screen.height/2), Input.mousePosition)) * vel_v * Time.deltaTime);
                segue_s.target_v = null;
            }
            if (mp.y <= margim)
            {
                transform.Translate(Vector3.down * (Vector2.Distance(new Vector2(Screen.width / 2, Screen.height / 2), Input.mousePosition)) * vel_v * Time.deltaTime);
                segue_s.target_v = null;
            }
            if (mp.y >= Screen.height - margim)
            {
                transform.Translate(Vector3.up * (Vector2.Distance(new Vector2(Screen.width / 2, Screen.height / 2), Input.mousePosition)) * vel_v * Time.deltaTime);
                segue_s.target_v = null;
            }

            if (mp.x >= Screen.width- margim)
            {
                transform.Translate(Vector3.right * (Vector2.Distance(new Vector2(Screen.width / 2, Screen.height / 2), Input.mousePosition)) * vel_v * Time.deltaTime);
                segue_s.target_v = null;
            }*/


        }
}
